//
//  UIColors.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 07/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

enum UIColors {
    static let darkBlue = UIColor(hexString: "#032467")
    static let blue = UIColor(hexString: "#003399")
    static let lighterBlue = UIColor(hexString: "#D0DBF0")
    static let lightestBlue = UIColor(hexString: "#E4EBF7")
    static let yellow = UIColor(hexString: "#FFCC00")
    static let red = UIColor(hexString: "#B62B36")
    static let darkGray = UIColor(hexString: "#707070")
}
