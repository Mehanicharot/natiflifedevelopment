//
//  UserInformationViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 12/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol NotificationStatusDelegate {
    func didChangeNotificationStatus(_ controller: UserInformationViewController, id: String, status: Bool)
}

class UserInformationViewController: UIViewController {
    var user: UserList.Response.ResponseData?

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var notificationStatusLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!

    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var alarmButton: UIButton!

    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    
    var delegate: NotificationStatusDelegate?

    var userDetails: UserData.Response?
    var notifications: Notification.Response?
    var selectedNotification: Notification.Response.ResponseData?

    var type: PickedType = .notifications {
        didSet {
            loadData(for: self.type)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }

    override func viewDidLoad() {
        super.viewDidLoad()

        notificationButton.isSelected = true
        setupButtons()

        let token = ActiveUser.shared.userInfo?.token ?? ""
        let id = user?.ID_Usr ?? ""

        switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        WebAPIController.shared.getUserData(token: token, id: id, completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let data):
                    guard let userInfo = data else { return }
                    self.userDetails = userInfo
                    self.prepareView()
                    self.type = .notifications
                case .failure(let error):
                    print("Error: \(error.message ?? "Unknown")")
                }
            }
        })
    }
    
    @objc
    private func switchChanged(mySwitch: UISwitch) {
        let value = switchControl.isOn
        notificationStatusLabel.text = switchControl.isOn ? "Notification: Enable" : "Notification: Disable"
        let requestData = NotificationStatus.Request(token: ActiveUser.shared.userInfo?.token ?? "", id_User: user?.ID_Usr ?? "", notify_status: "\(value)")
        WebAPIController.shared.setNotificationStatus(data: requestData, completion: { _ in })
        delegate?.didChangeNotificationStatus(self, id: user?.ID_Usr ?? "", status: switchControl.isOn)
    }

    private func setupButtons() {
        notificationButton.backgroundColor = notificationButton.isSelected ? UIColors.darkBlue : UIColors.lighterBlue
        notificationButton.setTitleColor(notificationButton.isSelected ? .white : UIColors.darkBlue, for: .normal)
        alarmButton.backgroundColor = alarmButton.isSelected ? UIColors.darkBlue : UIColors.lighterBlue
        alarmButton.setTitleColor(alarmButton.isSelected ? .white : UIColors.darkBlue, for: .normal)
    }

    @IBAction func didPressNotification(_ sender: Any) {
        notificationButton.isSelected = true
        alarmButton.isSelected = false
        setupButtons()

        type = .notifications
    }

    @IBAction func didPressAlarms(_ sender: Any) {
        notificationButton.isSelected = false
        alarmButton.isSelected = true
        setupButtons()

        type = .alarms
    }

    func prepareView() {
        guard let item = userDetails?.data?.first else { return }
        idLabel.text = "\(user?.ID_Usr ?? "")"
        ageLabel.text = "Age: \(item.Usr_Age)"
        heightLabel.text = "Height: \(item.Usr_Height)"
        weightLabel.text = "Weight: \(item.Usr_Weight)"
        heartRateLabel.text = "Heart Rate:\(item.heart_rate ?? "/")"
        stepsLabel.text = "Steps: \(item.steps ?? "/")"
        distanceLabel.text = "Distance: \(item.distance ?? "/")"
        image.image = UIImage(named: item.Usr_Gnd == "M" ? "patient-man" : "patient-girl")
        switchControl.isOn = item.Status == "true"
    }

    private func loadData(for type: PickedType) {
        loadingView.startAnimating()
        switch type {
        case .notifications:
            notifications = nil
            notificationTableView.reloadData()
            WebAPIController.shared.getNotificationForUser(with: user?.ID_Usr ?? "", type: "notification", completion: { results in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    switch results {
                    case .success(let list):
                        guard let notifications = list else { return }
                        self.loadingView.stopAnimating()
                        self.notifications = notifications
                        self.refactorNotificationData()
                        self.notificationTableView.reloadData()
                    case .failure(let error):
                        print("Error: \(error.message ?? "Unknown")")
                    }
                }
            })
        case .alarms:
            notifications = nil
            notificationTableView.reloadData()
            WebAPIController.shared.getNotificationForUser(with: user?.ID_Usr ?? "", type: "alarm", completion: { results in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    switch results {
                    case .success(let list):
                        guard let notifications = list else { return }
                        self.loadingView.stopAnimating()
                        self.notifications = notifications
                        self.refactorNotificationData()
                        self.notificationTableView.reloadData()
                    case .failure(let error):
                        print("Error: \(error.message ?? "Unknown")")
                    }
                }
            })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NotificationDetails" {
            if let destination = segue.destination as? NotificationDetailsViewController {
                destination.notification = selectedNotification
                destination.type = type
            }
        }
    }

    func refactorNotificationData() {
        for (index, notification) in notifications?.data.enumerated() ?? [].enumerated() {
            let str = notification.description
            let data = Data(str.utf8)

            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    // try to read out a string array
                    var detail = Detail()

                    if let sensor = json["Sensore"] as? String {
                        detail.sensor = sensor
                    }

                    if let data = json["Data"] as? String {
                        detail.date = data
                    }

                    if let status = json["Stato"] as? String {
                        detail.status = status
                    }

                    notifications?.data[index].setDetail(detail)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
    }
}

extension UserInformationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let notification = self.notifications else { return 0 }
        return notification.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell else { return UITableViewCell() }

        let item = notifications?.data[indexPath.row]

        cell.notificationNameLabel.text = item?.detail?.sensor
        cell.shortDescriptionLabel.text = "Take in charge: \(item?.take_charge ?? "" == "true" ? "YES" : "NO")"
        cell.timeLabel.text = item?.delivery
        cell.prepare(for: type)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedNotification = notifications?.data[indexPath.row]
        performSegue(withIdentifier: "NotificationDetails", sender: nil)
    }
}
