//
//  UsersViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

class UsersViewController: UIViewController {
    var userList: [UserList.Response.ResponseData] = []
    private var selectedUser: UserList.Response.ResponseData?
    
    @IBOutlet weak var usersTableView: UITableView!
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        userList = ActiveUser.shared.userList?.data ?? []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Users"
    }
}

extension UsersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell else {
            return UITableViewCell()
        }

        let item = userList[indexPath.row]

        cell.idLabel.text = item.ID_Usr
        cell.shortDescriptionLabel.text = "Notification: \(item.Status == "true" ? "Enable" : "Disable")"
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedUser = userList[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "UserInformations", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UserInformations" {
            if let destination = segue.destination as? UserInformationViewController {
                destination.user = selectedUser
                destination.delegate = self
            }
        }
    }
}

extension UsersViewController: NotificationStatusDelegate {
    func didChangeNotificationStatus(_ controller: UserInformationViewController, id: String, status: Bool) {
        if let row = self.userList.firstIndex(where: {$0.ID_Usr == id}) {
            userList[row].Status = status ? "true" : "false"
            self.usersTableView.reloadData()
        }
    }
}

class UserCell: UITableViewCell {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var shortDescriptionLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
}
