//
//  NotificationDetailsViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 12/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

protocol TakeInChargeDelegate {
    func didChangeStatus(_ controller: NotificationDetailsViewController, id: String, status: Bool)
}

class NotificationDetailsViewController: UIViewController {
    var notification: Notification.Response.ResponseData?
    var type: PickedType?

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var moreDetailsLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    var delegate: TakeInChargeDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }

    func prepareView() {
        nameLabel.text = notification?.detail?.sensor ?? ""
        descriptionLabel.text = "Take in charge: \((notification?.take_charge ?? "") == "false" ? "NO" : "YES")"
        timeLabel.text = notification?.delivery ?? ""
        switchControl.isOn = notification?.take_charge == "true"

        colorView.backgroundColor = type == .notifications ? UIColors.yellow : UIColors.red
        self.title = type == .notifications ? "Notification" : "Alarm"
        
        switchControl.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc
    private func switchChanged(mySwitch: UISwitch) {
        guard let token = ActiveUser.shared.userInfo?.token else { return }
        WebAPIController.shared.takeInChargeRequest(token: token, id: notification?.id ?? "", chargeStatus: mySwitch.isOn, completion: { _ in })
        delegate?.didChangeStatus(self, id: self.notification?.id ?? "", status: mySwitch.isOn)
    }

    @IBAction func switchControlChange(_ sender: Any) {
        guard let control = sender as? UISwitch else { return }
        descriptionLabel.text = control.isOn ? "Take in charge: YES" : "Take in charge: NO"
    }
}
