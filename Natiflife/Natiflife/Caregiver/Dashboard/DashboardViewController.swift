//
//  DashboardViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 07/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

enum PickedType {
    case notifications
    case alarms
}

class DashboardViewController: UIViewController {
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var joinedLabel: UILabel!
    @IBOutlet weak var yearsWorkingLabel: UILabel!
    @IBOutlet weak var otherInfoLabel: UILabel!
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    @IBOutlet weak var notificationTableView: UITableView!

    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var alarmsButton: UIButton!

    var notifications: Notification.Response?
    var selectedNotification: Notification.Response.ResponseData?
    var type: PickedType = .notifications {
        didSet {
            loadData(for: self.type)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        notificationButton.isSelected = true
        type = .notifications
        firstnameLabel.text = ActiveUser.shared.username
        setupButtons()

        if ActiveUser.shared.type == .caregiver {
            WebAPIController.shared.getUserList(token: ActiveUser.shared.userInfo?.token ?? "", completion: { results in
                DispatchQueue.main.async {
                    switch results {
                    case .success(let users):
                        guard let data = users else { return }
                        ActiveUser.shared.userList = data
                    case .failure(let error):
                       print("Error: \(error.message ?? "Unknown")")
                    }
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Dashboard"
    }
    
    @IBAction func didPressNotification(_ sender: Any) {
        notificationButton.isSelected = true
        alarmsButton.isSelected = false
        setupButtons()

        type = .notifications
    }

    @IBAction func didPressAlarms(_ sender: Any) {
        notificationButton.isSelected = false
        alarmsButton.isSelected = true
        setupButtons()

        type = .alarms
    }

    private func setupButtons() {
        notificationButton.backgroundColor = notificationButton.isSelected ? UIColors.darkBlue : UIColors.lighterBlue
        notificationButton.setTitleColor(notificationButton.isSelected ? .white : UIColors.darkBlue, for: .normal)
        alarmsButton.backgroundColor = alarmsButton.isSelected ? UIColors.darkBlue : UIColors.lighterBlue
        alarmsButton.setTitleColor(alarmsButton.isSelected ? .white : UIColors.darkBlue, for: .normal)
    }

    private func loadData(for type: PickedType) {
        loadingView.startAnimating()
        switch type {
        case .notifications:
            notifications = nil
            notificationTableView.reloadData()
            if ActiveUser.shared.type == .caregiver {
                WebAPIController.shared.listOfNotification(type: "notification", completion: { results in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        switch results {
                        case .success(let list):
                            guard let notifications = list else { return }
                            self.loadingView.stopAnimating()
                            self.notifications = notifications
                            self.refactorNotificationData()
                            self.notificationTableView.reloadData()
                        case .failure(let error):
                            print("Error: \(error.message ?? "Unknown")")
                        }
                    }
                })
            } else {
                WebAPIController.shared.getNotificationForUser(with: ActiveUser.shared.userInfo?.id ?? "", type: "notification", completion: { results in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        switch results {
                        case .success(let list):
                            guard let notifications = list else { return }
                            self.loadingView.stopAnimating()
                            self.notifications = notifications
                            self.refactorNotificationData()
                            self.notificationTableView.reloadData()
                        case .failure(let error):
                            print("Error: \(error.message ?? "Unknown")")
                        }
                    }
                })
            }
        case .alarms:
            if ActiveUser.shared.type == .caregiver {
                WebAPIController.shared.listOfNotification(type: "alarm", completion: { results in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        switch results {
                        case .success(let list):
                            guard let notifications = list else { return }
                            self.loadingView.stopAnimating()
                            self.notifications = notifications
                            self.refactorNotificationData()
                            self.notificationTableView.reloadData()
                        case .failure(let error):
                            print("Error: \(error.message ?? "Unknown")")
                        }
                    }
                })
            } else {
                
                WebAPIController.shared.getNotificationForUser(with: ActiveUser.shared.userInfo?.id ?? "", type: "alarm", completion: { results in
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        switch results {
                        case .success(let list):
                            guard let notifications = list else { return }
                            self.loadingView.stopAnimating()
                            self.notifications = notifications
                            self.refactorNotificationData()
                            self.notificationTableView.reloadData()
                        case .failure(let error):
                            print("Error: \(error.message ?? "Unknown")")
                        }
                    }
                })
            }
        }
    }

    func refactorNotificationData() {
        for (index, notification) in notifications?.data.enumerated() ?? [].enumerated() {
            let str = notification.description
            let data = Data(str.utf8)

            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    // try to read out a string array
                    var detail = Detail()

                    if let sensor = json["Sensore"] as? String {
                        detail.sensor = sensor
                    }

                    if let data = json["Data"] as? String {
                        detail.date = data
                    }

                    if let status = json["Stato"] as? String {
                        detail.status = status
                    }

                    notifications?.data[index].setDetail(detail)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NotificationDetails" {
            if let destination = segue.destination as? NotificationDetailsViewController {
                destination.notification = selectedNotification
                destination.type = type
                destination.delegate = self
            }
        }
    }
}

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let notification = self.notifications else { return 0 }
        return notification.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell else { return UITableViewCell() }

        let item = notifications?.data[indexPath.row]

        cell.notificationNameLabel.text = item?.detail?.sensor
        cell.shortDescriptionLabel.text = "Take in charge: \((item?.take_charge ?? "") == "false" ? "NO" : "YES")"
        cell.timeLabel.text = item?.delivery
        cell.prepare(for: type)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ActiveUser.shared.type == .patient { return }
        tableView.deselectRow(at: indexPath, animated: true)
        selectedNotification = notifications?.data[indexPath.row]
        performSegue(withIdentifier: "NotificationDetails", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        ActiveUser.shared.type == .caregiver
    }
}

extension DashboardViewController: TakeInChargeDelegate {
    func didChangeStatus(_ controller: NotificationDetailsViewController, id: String, status: Bool) {
        if let row = self.notifications?.data.firstIndex(where: { $0.id == id }) {
            notifications?.data[row].take_charge = status ? "true" : "false"
            self.notificationTableView.reloadData()
        }
    }
}

class NotificationCell: UITableViewCell {
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var notificationNameLabel: UILabel!
    @IBOutlet weak var shortDescriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    func prepare(for type: PickedType) {
        sideView.backgroundColor = type == .alarms ? UIColors.red : UIColors.yellow
    }
}
