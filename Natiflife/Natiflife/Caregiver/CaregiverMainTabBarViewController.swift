//
//  CaregiverMainTabBarViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 14/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit
import CocoaMQTT

class CaregiverMainTabBarViewController: UITabBarController {    
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.unselectedItemTintColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    @IBAction func logoutPress(_ sender: Any) {
        ActiveUser.shared.logout()
        ActiveUser.shared.userInfo = nil
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func controlRoomPressed(_ sender: Any) {
        let controller = UIStoryboard(name: "CaregiverTabBar", bundle: nil).instantiateViewController(identifier: "ControlRoomID")
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
