//
//  PatientControlViewController.swift
//  Natiflife
//
//  Created by Martin on 8/14/20.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

class PatientControlViewController: UIViewController {
    @IBOutlet weak var lightsTableView: UITableView!
    var roomResponse: Room.Response?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WebAPIController.shared.getPatientLightsList(completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let response):
                    self.roomResponse = response
                    self.lightsTableView.reloadData()
                case .failure(let error):
                    print("Error: \(error.message ?? "Unknown")")
                }
            }
        })
    }
    
    @IBAction func panicButtonPressed(_ sender: Any) {
        guard let token = ActiveUser.shared.userInfo?.token else {
            return
        }
        WebAPIController.shared.sendPanicSignal(token: token, type: "panic", completion: { _ in})
        MQTTService.shared.postPanicButton()
    }
    
    @IBAction func comeToRoomButtonPressed(_ sender: Any) {
        guard let token = ActiveUser.shared.userInfo?.token else {
            return
        }
        WebAPIController.shared.sendPanicSignal(token: token, type: "come", completion: { _ in})
    }
}

extension PatientControlViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomResponse?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PatientLightCellIdentifier", for: indexPath) as? PatientLightCell else {
            return UITableViewCell()
        }
        let item = roomResponse?.data?[indexPath.row]
        
        cell.lightInfo = item
        cell.position = indexPath.row
        return cell
    }
    
    
}

class PatientLightCell: UITableViewCell {
    enum LightButton {
        case on
        case off
        
        static func status(_ status: String) -> LightButton {
            if status == "ON" {
                return LightButton.on
            }
            
            return LightButton.off
        }
    }
    @IBOutlet weak var lightBullImage: UIImageView!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var offButton: UIButton!
    @IBOutlet weak var onButton: UIButton!
    
    var activeButton: LightButton?
    var lightInfo: Room.ResponseData?
    
    var position: Int?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        activeButton = LightButton.status(lightInfo?.Status ?? "OFF")
        roomNameLabel.text = lightInfo?.Room
        setupActiveButtons(activeButton: activeButton ?? .off)
    }
    
    @IBAction func offButtonPressed(_ sender: Any) {
        setupActiveButtons(activeButton: .off)
        MQTTService.shared.powerOffLight(position: position ?? 0)
    }
    
    @IBAction func onButtonPressed(_ sender: Any) {
        setupActiveButtons(activeButton: .on)
        MQTTService.shared.powerOnLight(position: position ?? 0)
    }
    
    private func setupActiveButtons(activeButton: LightButton) {
        switch activeButton {
        case .off:
            offButton.backgroundColor = UIColors.darkBlue
            offButton.setTitleColor(.white, for: .normal)
            onButton.backgroundColor = nil
            onButton.setTitleColor(UIColors.darkBlue, for: .normal)
            lightBullImage.image = UIImage(named: "lightbulb-off")
        case .on:
            offButton.backgroundColor = nil
            offButton.setTitleColor(UIColors.darkBlue, for: .normal)
            onButton.backgroundColor = UIColors.darkBlue
            onButton.setTitleColor(.white, for: .normal)
            lightBullImage.image = UIImage(named: "lightbulb_on")
        }
    }
}
