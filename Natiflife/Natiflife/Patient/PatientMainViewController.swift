//
//  PatientMainViewController.swift
//  Natiflife
//
//  Created by Martin on 8/13/20.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

class PatientMainViewController: UITabBarController {
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBar.unselectedItemTintColor = UIColor.white
    }

    @IBAction func logoutPress(_ sender: Any) {
        ActiveUser.shared.logout()
        ActiveUser.shared.userInfo = nil
        self.dismiss(animated: true, completion: nil)
    }
}
