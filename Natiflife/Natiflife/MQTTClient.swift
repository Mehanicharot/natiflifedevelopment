//
//  MQTTClient.swift
//  Natiflife
//
//  Created by Martin on 8/18/20.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation
import CocoaMQTT

class MQTTService {
    static let shared = MQTTService()
    
    private let ipAddress = ActiveUser.shared.userInfo?.mqttIP ?? ""
    private let port = UInt16(ActiveUser.shared.userInfo?.mqttPort ?? "8080") ?? 8080
    
    private let clientID = "clientIdDemoMqtt"
    private let subscriptionTopic = "cmnd/APPCG1/+"
    
    private var mqtt: CocoaMQTT?
    
    private init() {
        mqttSetting()
    }
    
    private func mqttSetting() {
        mqtt = CocoaMQTT(clientID: clientID, host: ipAddress, port: port)
        mqtt?.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
        mqtt?.keepAlive = 60
        mqtt?.delegate = self
        connect()
    }
    
    private func connect() {
        guard let mqtt = self.mqtt else { return }
        print("Connect Status: \(mqtt.connect())")
    }
    
    func powerOffLight(position: Int) {
        guard let mqtt = self.mqtt else { return }
        let topic = "stat/APPCG1/\(position)"
        let messageValue = "{POWER:OFF}"
        let message = CocoaMQTTMessage(topic: topic, string: messageValue)
        
        mqtt.publish(message)
    }
    
    func powerOnLight(position: Int) {
        guard let mqtt = self.mqtt else { return }
        let topic = "stat/APPCG1/\(position)"
        let messageValue = "{POWER:ON}"
        let message = CocoaMQTTMessage(topic: topic, string: messageValue)
        
        mqtt.publish(message)
    }
    
    func postPanicButton() {
        guard let mqtt = self.mqtt else { return }
        let timestamp = Date().timeIntervalSince1970
        let userId = ActiveUser.shared.userInfo?.id ?? ""
        let messageValue = "{\"TimeStamp\": \"\(timestamp)\", \"USERID\", \"\(userId)\", \"Button\": \"true\"}"
        let message = CocoaMQTTMessage(topic: "Alert/PanicButton/", string: messageValue)
        
        mqtt.publish(message)
    }
}

extension MQTTService: CocoaMQTTDelegate {
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
            print("ack: \(ack)")

            if ack == .accept {
    //            mqtt.subscribe("chat/room/animals/client/+", qos: CocoaMQTTQoS.qos1)
    //
    //            let chatViewController = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
    //            chatViewController?.mqtt = mqtt
    //            navigationController!.pushViewController(chatViewController!, animated: true)
            }
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didStateChangeTo state: CocoaMQTTConnState) {
            print("new state: \(state)")
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
            print("message: \(String(describing: message.string?.description)), id: \(id)")
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
            print("id: \(id)")
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
            print("message: \(String(describing: message.string?.description)), id: \(id)")

    //        let name = NSNotification.Name(rawValue: "MQTTMessageNotification" + animal!)
    //        NotificationCenter.default.post(name: name, object: self, userInfo: ["message": message.string!, "topic": message.topic])
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopics success: NSDictionary, failed: [String]) {
            print("subscribed: \(success), failed: \(failed)")
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topics: [String]) {
            print("topic: \(topics)")
        }
        
        func mqttDidPing(_ mqtt: CocoaMQTT) {
            print()
        }
        
        func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
            print()
        }

        func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
            print("\(String(describing: err?.localizedDescription))")
        }
        
        func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
            print("Unsubscribe on topic: \(topic)")
        }
}
