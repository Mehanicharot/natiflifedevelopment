//
//  ActiveUser.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

class ActiveUser {
    static let shared = ActiveUser()
    private let storage = UserStorage()

    var userInfo: ActiveUserInfo?
    var userList: UserList.Response?

    var alreadyLogin: Bool {
        return !username.isEmpty && !password.isEmpty
    }

    var username: String {
        return storage.retriveUsername() ?? ""
    }

    var password: String {
        return storage.retrivePassword() ?? ""
    }

    var type: UserType {
        return storage.retriveType() == "patient" ? .patient : .caregiver
    }

    private init() { }

    func setOneSignalToken(_ value: String) {
        storage.storeOneSignalToken(value)
    }

    func setLoginData(usernma: String, password: String, type: String) {
        storage.storeUsername(usernma)
        storage.storePassword(password)
        storage.storeType(type)
    }

    func getOneSignalToken() -> String {
        return storage.retriveOneSignalToken() ?? ""
    }

    func setLoginUser(_ userData: Login.ResponseCaregiver) {
        let token = userData.data.userToken
        let role: ActiveUserInfo.UserRole = ActiveUserInfo.UserRole(rawValue: Int(userData.data.userRole) ?? 3) ?? .caregiver
        let ip = userData.data.mqtt_ip
        let port = userData.data.port

        self.userInfo = ActiveUserInfo(id: "", token: token, role: role, mqttIP: ip, mqttPort: port, deviceId: "")
    }

    func setLoginUser(_ userData: Login.ResponsePartient) {
        let token = userData.data.token
        let role: ActiveUserInfo.UserRole = .patient

        self.userInfo = ActiveUserInfo(id: "", token: token, role: role, mqttIP: "", mqttPort: "", deviceId: "")
    }

    func logout() {
        storage.removeAllUserData()
    }
}
