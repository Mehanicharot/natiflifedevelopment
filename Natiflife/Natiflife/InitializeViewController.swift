//
//  ViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 04/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit

class InitializeViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !ActiveUser.shared.alreadyLogin {
            performSegue(withIdentifier: "Login", sender: nil)
        } else {
            let username = ActiveUser.shared.username
            let password = ActiveUser.shared.password
            let login = Login.Request(username: username, password: password)

            let semaphore = DispatchSemaphore(value: 0)

            if ActiveUser.shared.type == .caregiver {
                WebAPIController.shared.loginRequest(login, completion: { results in
                    semaphore.signal()
                    DispatchQueue.main.async {
                        switch results {
                        case .success(let user):
                            guard let data = user else { return }
                            ActiveUser.shared.setLoginUser(data)
                            self.setCaregiverDeviceID()
                            WebAPIController.shared.getUserData(token: ActiveUser.shared.userInfo?.token ?? "" , id: ActiveUser.shared.userInfo?.id ?? "", completion: { user in
                            })
                        case .failure(let error):
                            AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                        }
                    }
                })
            } else {
                WebAPIController.shared.loginPatientRequest(login, completion: { results in
                    semaphore.signal()
                    DispatchQueue.main.async {
                        switch results {
                        case .success(let user):
                            guard let data = user else { return }
                            ActiveUser.shared.setLoginUser(data)
                            let type = "patient"
                            ActiveUser.shared.setLoginData(usernma: username, password: password, type: type)
                            self.setPatiendDeviceID()
                        case .failure(let error):
                            AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                        }
                    }
                })
            }
        }
    }

    private func setCaregiverDeviceID() {
        let token = ActiveUser.shared.userInfo?.token ?? ""
        let deviceId = ActiveUser.shared.getOneSignalToken()
        let deviceRequest = Device.Request(token: token, device_id: deviceId)

        WebAPIController.shared.deviceIDRequest(deviceRequest, completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let userInfo):
                    guard let data = userInfo else { return }
                    ActiveUser.shared.userInfo?.id = data.data.Id_Caregiver
                    self.performSegue(withIdentifier: "CaregiverMain", sender: nil)
                case .failure(let error):
                   AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                }
            }

        })
    }
    
    private func setPatiendDeviceID() {
        let token = ActiveUser.shared.userInfo?.token ?? ""
        let deviceId = ActiveUser.shared.getOneSignalToken()
        let deviceRequest = Device.Request(token: token, device_id: deviceId)

        WebAPIController.shared.deviceIDPatientRequest(deviceRequest, completion: { results in
              DispatchQueue.main.async {
                  switch results {
                  case .success(let userInfo):
                      guard let data = userInfo else { return }
                      ActiveUser.shared.userInfo?.id = data.data.Id_User
                      self.performSegue(withIdentifier: "PatientMain", sender: nil)
                  case .failure(let error):
                     AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                  }
              }
        })
    }
}

