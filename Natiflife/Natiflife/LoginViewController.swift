//
//  LoginViewController.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import UIKit
import UICheckbox_Swift

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var checkbox: UICheckbox!

    var isCheckboxSelected: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        checkbox.onSelectStateChanged = { (checkbox, selected) in
            self.isCheckboxSelected = selected
        }
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        let username = self.username.text ?? ""
        let password = self.password.text ?? ""
        let login = Login.Request(username: username, password: password)

        DispatchQueue.main.async {
            let semaphore = DispatchSemaphore(value: 1)

            if self.isCheckboxSelected {
                WebAPIController.shared.loginRequest(login, completion: { results in
                    semaphore.signal()
                    DispatchQueue.main.async {
                        switch results {
                        case .success(let user):
                            guard let data = user else { return }
                            ActiveUser.shared.setLoginUser(data)
                            let type = "caregiver"
                            ActiveUser.shared.setLoginData(usernma: username, password: password, type: type)
                            self.setCaregiverDeviceID()
                        case .failure(let error):
                            AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                        }
                    }
                })
            } else {
                WebAPIController.shared.loginPatientRequest(login, completion: { results in
                    semaphore.signal()
                    DispatchQueue.main.async {
                        switch results {
                        case .success(let user):
                            guard let data = user else { return }
                            ActiveUser.shared.setLoginUser(data)
                            let type = "patient"
                            ActiveUser.shared.setLoginData(usernma: username, password: password, type: type)
                            self.setPatiendDeviceID()
                        case .failure(let error):
                            AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                        }
                    }
                })
            }
        }
    }

    private func performSegue(for type: UserType) {
        if let id = ActiveUser.shared.userInfo?.id, !id.isEmpty {
            if isCheckboxSelected {
                self.performSegue(withIdentifier: "CaregiverMain", sender: nil)
            } else {
                self.performSegue(withIdentifier: "PatientMain", sender: nil)
            }
        }
    }
    
    private func setCaregiverDeviceID() {
        let token = ActiveUser.shared.userInfo?.token ?? ""
        let deviceId = ActiveUser.shared.getOneSignalToken()
        let deviceRequest = Device.Request(token: token, device_id: deviceId)

        WebAPIController.shared.deviceIDRequest(deviceRequest, completion: { results in
            DispatchQueue.main.async {
                switch results {
                case .success(let userInfo):
                    guard let data = userInfo else { return }
                    ActiveUser.shared.userInfo?.id = data.data.Id_Caregiver
                    self.performSegue(for: .caregiver)
                case .failure(let error):
                   AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
                }
            }

        })
    }
    
    private func setPatiendDeviceID() {
        let token = ActiveUser.shared.userInfo?.token ?? ""
        let deviceId = ActiveUser.shared.getOneSignalToken()
        let deviceRequest = Device.Request(token: token, device_id: deviceId)

        WebAPIController.shared.deviceIDPatientRequest(deviceRequest, completion: {  results in
           DispatchQueue.main.async {
               switch results {
               case .success(let userInfo):
                   guard let data = userInfo else { return }
                   ActiveUser.shared.userInfo?.id = data.data.Id_User
                   self.performSegue(for: .patient)
               case .failure(let error):
                  AlertManager.shared.showBasicAlert(self, message: error.message ?? "Unknown")
               }
           }
        })
    }
}
