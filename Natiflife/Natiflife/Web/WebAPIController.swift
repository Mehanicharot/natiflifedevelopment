//
//  WebApiController.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

enum UserType {
    case caregiver
    case patient
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

class WebAPIController {
    static let shared = WebAPIController(timeout: 30)

    // MARK: - Private Properties
    private let urlSession: URLSession
    private let encoder = JSONEncoder()
    private let decoder = JSONDecoder()
    private let baseApiUrl = "https://platform.natiflife-project.eu/notika/"
    private lazy var loginCaregiverApi = "\(baseApiUrl)mobile/login.php"
    private lazy var loginUtentiApi = "\(baseApiUrl)mobile/login_utenti.php"
    private lazy var deviceCaregiverApi = "\(baseApiUrl)mobile/id_device.php"
    private lazy var deviceUtentiApi = "\(baseApiUrl)mobile/users_device.php"
    private lazy var usersListApi = "\(baseApiUrl)mobile/lista_utenti.php"
    private lazy var notificationStatusApi = "\(baseApiUrl)mobile/abilita_notifiche.php"
    private lazy var notificationsListApi = "\(baseApiUrl)mobile/list_alarm.php"
    private lazy var lightStatusApi = "\(baseApiUrl)mobile/light_status.php"
    private lazy var patientLightStatusApi = "\(baseApiUrl)mobile/light_status_user.php"
    private lazy var patientButtonsApi = "\(baseApiUrl)mobile/request.php"
    private lazy var takeInChargeApi = "\(baseApiUrl)mobile/presa_in_carico.php"
    


    // MARK: - Init
    private init (timeout: TimeInterval = 60) {
        let configuration = URLSessionConfiguration.default

        configuration.timeoutIntervalForRequest = timeout
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.httpShouldSetCookies = false
        configuration.waitsForConnectivity = false

        urlSession = URLSession(configuration: configuration)
    }

    func loginRequest(_ data: Login.Request, completion: @escaping ((Result<Login.ResponseCaregiver?, APIError>) -> Void)) {
        guard let url = URL(string: loginCaregiverApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "username": data.username,
            "password": data.password
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }

    func loginPatientRequest(_ data: Login.Request, completion: @escaping ((Result<Login.ResponsePartient?, APIError>) -> Void)) {
        guard let url = URL(string: loginUtentiApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "ID": data.username,
            "password": data.password
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }

    func deviceIDRequest(_ data: Device.Request, completion: @escaping ((Result<Device.Response?, APIError>) -> Void)) {
        guard let url = URL(string: deviceCaregiverApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": data.token,
            "device_id": data.device_id
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }

    func deviceIDPatientRequest(_ data: Device.Request, completion: @escaping ((Result<Device.ResponsePatient?, APIError>) -> Void)) {
        guard let url = URL(string: deviceUtentiApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": data.token,
            "device_id": data.device_id
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }

    func listOfNotification(type: String, completion: @escaping ((Result<Notification.Response?, APIError>) -> Void)) {
        guard let url = URL(string: "\(notificationsListApi)?type=\(type)") else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue

        dataTask(with: request, completion: completion)
    }

    func getNotificationForUser(with id: String, type: String, completion: @escaping ((Result<Notification.Response?, APIError>) -> Void)) {
        guard let url = URL(string: "\(notificationsListApi)?user_id=\(id)&type=\(type)") else { fatalError("Cannot make url with endpoint!") }

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue

        dataTask(with: request, completion: completion)
    }

    func getUserList(token: String, completion: @escaping ((Result<UserList.Response?, APIError>) -> Void)) {
        guard let url = URL(string: usersListApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": token,
            "type": "list"
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }

    func getUserData(token: String, id: String, completion: @escaping ((Result<UserData.Response?, APIError>) -> Void)) {
        guard let url = URL(string: usersListApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "type": "data",
            "token": token,
            "Id_user": id
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }
    
    func setNotificationStatus(data: NotificationStatus.Request, completion: @escaping ((Result<NotificationStatus.Response?, APIError>) -> Void)) {
        guard let url = URL(string: notificationStatusApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": data.token,
            "notify_status": data.notify_status,
            "id_User": data.id_User
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()

        dataTask(with: request, completion: completion)
    }
    
    func getLightsList(completion: @escaping ((Result<Room.Response?, APIError>) -> Void)) {
        guard let url = URL(string: lightStatusApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": ActiveUser.shared.userInfo?.token ?? ""
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()
        
        dataTask(with: request, completion: completion)
    }
    
    func getPatientLightsList(completion: @escaping ((Result<Room.Response?, APIError>) -> Void)) {
        guard let url = URL(string: patientLightStatusApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": ActiveUser.shared.userInfo?.token ?? ""
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()
        
        dataTask(with: request, completion: completion)
    }
    
    func sendPanicSignal(token: String, type: String, completion: @escaping ((Result<Room.Response?, APIError>) -> Void)) {
        guard let url = URL(string: patientButtonsApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": token,
            "type": type
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()
        
        dataTask(with: request, completion: completion)
    }
    
    func takeInChargeRequest(token: String, id: String, chargeStatus: Bool, completion: @escaping ((Result<Room.Response?, APIError>) -> Void)) {
        guard let url = URL(string: takeInChargeApi) else { fatalError("Cannot make url with endpoint!") }
        let parameters: [String: Any] = [
            "token": token,
            "id": id,
            "take_charge": "\(chargeStatus)"
        ]

        var request = URLRequest(url: url)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.networkServiceType = .responsiveData
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = parameters.percentEncoded()
        
        dataTask(with: request, completion: completion)
    }

    private func dataTask<JSONObject: Decodable>(with request: URLRequest, completion: @escaping ((Result<JSONObject?, APIError>) -> Void)) {
        let task = urlSession.dataTask(with: request) { [weak self] data, response, error in
            guard let data = data else {
                completion(.failure(APIError(error: .requestError)))
                return
            }

            var object: JSONObject?

            do {
                guard let stringAlert = String(data: data, encoding: .utf8)?.replacingOccurrences(of: "<br />\n<b>Notice</b>:  Undefined index: page in <b>/var/www/vhosts/platform.natiflife-project.eu/httpdocs/notika/mobile/list_alarm.php</b> on line <b>21</b><br />\n", with: "") else { return }
                guard let realData = stringAlert.data(using: .utf8) else { return }
                if let responseError = try self?.decoder.decode(APIError.self, from: realData) {
                    if responseError.hasError {
                        completion(.failure(responseError))
                        return
                    } else {
                        object = try self?.decoder.decode(JSONObject.self, from: realData)
                    }
                }
            } catch {
                print("Decoding Error: \(error)")
            }

            guard let jsonObject = object else {
                completion(.failure(APIError(error: .jsonNotValid)))
                return
            }

            completion(.success(jsonObject))
        }

        task.resume()
    }
}

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
