//
//  WebAPIError.swift
//  Mobilitas
//
//  Created by Martin . Andonovski on 28/01/2020.
//  Copyright © 2020 Martin . Andonovski. All rights reserved.
//

import Foundation

struct APIError: Codable, Error {
    enum ErrorCode: Int {
        case ok = 1
        case wrongPassword = -1
        case callAdministrator = -2
        case wrongUsername = -3
        case wronToken = -4
        case requestError = -5
        case unknown = -6
        case jsonNotValid = -7

        func message() -> String {
            switch self {
            case .ok:
                return "OK, no error"
            case .wrongPassword:
                return "Wrong password"
            case .callAdministrator:
                return "Error, call administrator"
            case .wrongUsername:
                return "Wrong username"
            case .wronToken:
                return "Wrong token"
            case .requestError:
                return "Request Error"
            case .unknown:
                return "Unknown Error"
            case .jsonNotValid:
                return "JSON is not valid"
            }
        }
    }

    var code: Int
    var message: String? {
        return errorCode.message()
    }
    var errorCode: ErrorCode {
        return ErrorCode(rawValue: code) ?? .unknown
    }
    var hasError: Bool {
        return errorCode != .ok
    }

    init(error: ErrorCode) {
        code = error.rawValue
    }
}
