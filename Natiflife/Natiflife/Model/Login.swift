//
//  Login.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 04/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum Login {
    struct Request: Codable {
        var username: String
        var password: String
    }

    struct ResponseCaregiver: Codable {
        var code: Int
        var data: ResponseData
    }

    struct ResponsePartient: Codable {
        var code: Int
        var data: ResponsePartientData
    }

    struct ResponseData: Codable {
        var userToken: String
        var userRole: String
        var mqtt_ip: String
        var port: String
    }

    struct ResponsePartientData: Codable {
        var token: String
        var username: String
    }
}
