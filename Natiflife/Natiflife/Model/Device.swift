//
//  Device.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum Device {
    struct Request: Codable {
        var token: String
        var device_id: String
    }

    struct Response: Codable {
        var code: Int
        var data: ResponseData
    }

    struct ResponsePatient: Codable {
        var code: Int
        var data: ResponsePatientData
    }

    struct ResponsePatientData: Codable {
        var Id_User: String
    }

    struct ResponseData: Codable {
        var Id_Caregiver: String
    }
}
