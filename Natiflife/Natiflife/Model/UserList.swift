//
//  UserList.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 12/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum UserList {
    struct Request: Codable { }
    struct Response: Codable {
        var code: Int
        var data: [ResponseData]?

        struct ResponseData: Codable {
            var ID_Usr: String
            var Status: String
        }
    }
}

enum UserData {
    struct Request: Codable {}
    struct Response: Codable {
        var code: Int
        var data: [ResponseData]?

        struct ResponseData: Codable {
            var ID_RFID: String?
            var Usr_Age: String
            var Usr_Height: String
            var Usr_Weight: String
            var Usr_Gnd: String
            var HR_thMax: String
            var HR_thmin: String
            var SWB_TO: String
            var SWK_thmin: String
            var SWK_TO: String
            var heart_rate: String?
            var steps: String?
            var distance: String?
            var activity: String?
            var Status: String?
        }
    }
}
