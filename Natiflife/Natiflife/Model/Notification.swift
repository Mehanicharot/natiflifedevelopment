//
//  Notification.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum Notification {
    struct Request: Codable { }
    struct Response: Codable {
        var code: Int
        var data: [ResponseData]
        var details: [DetailsData]?

        struct ResponseData: Codable {
            var id: String
            var type: String
            var caregiver: String
            var user_id: String
            var description: String
            var delivery: String
            var take_charge: String
            var take_charge_date: String

            private(set) var detail: Detail?
            mutating func setDetail(_ value: Detail) {
                detail = value
            }
        }

        struct DetailsData: Codable {
            var current_page: Int
            var total_page: Int?
            var total_row: Int
            var for_page: Int
        }

        struct Description: Codable {
            var sensore: String
            var data: String
            var stato: String
        }
    }
}

struct Detail: Codable {
    var sensor: String = ""
    var date: String = ""
    var status: String = ""
}
