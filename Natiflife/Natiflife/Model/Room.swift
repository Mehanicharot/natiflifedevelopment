//
//  Room.swift
//  Natiflife
//
//  Created by Martin on 8/13/20.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum Room {
    struct Response: Codable {
        var code: Int
        var data: [ResponseData]?
    }
    
    struct ResponseData: Codable {
        var Room: String?
        var Status: String?
    }
}
