//
//  UserInfo.swift
//  Natiflife
//
//  Created by Martin . Andonovski on 11/03/2020.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

struct ActiveUserInfo {
    enum UserRole: Int {
        case caregiver = 3
        case patient = 2
    }

    var id: String
    var token: String
    var role: UserRole
    var mqttIP: String
    var mqttPort: String
    var deviceId: String
}
