//
//  NotificationStatus.swift
//  Natiflife
//
//  Created by Martin on 8/13/20.
//  Copyright © 2020 Martin.Andonovski. All rights reserved.
//

import Foundation

enum NotificationStatus {
    struct Request: Codable {
        var token: String
        var id_User: String
        var notify_status: String
    }

    struct Response: Codable {
        var code: Int
        var data: ResponseData?
    }
    
    struct ResponseData: Codable {
        var Status: String?
    }
}
